## Task ##
Create a simple search page

### Conditions ###
Use Boataround API to create simple search page.
The technology, how to connect and manage data from Boataround API, is up to your choice,
but you have to give us an installation manual, how to run up your project.

### Helpers ###
Boataround API offers REST API and clean documentation to access all the endpoints.
* Full Docs - https://api.boataround.com/

Testing endpoint:
* https://api.boataround.com/v1/search?region=Zadar&page=1&checkIn=2019-07-20&checkOut=2019-07-27&lang=en_EN&sort=rank&currency=EUR

### Page conditions ###

 - page contains list of boats (mandatory)
 - working pagination (optional)
 - page contains calendar to change check-in and check-out date (optional)
 - page contains any kind of filter (optional)

### Delivery ###

1. Pack repository to .zip and send it to pavel@boataround.com and to miroslav.kostka@dev.boataround.com

## Conditional task ##

In case of any trouble with the technical part of the project, you are free to make conditional task: Boataround would like to create an App/Web for its customers on holidays in the Mediterranean sea.
App/Web will be showing restaurants, hotels, POI, etc. according to geodata - latitude, and longitude of the users, or user marinas. How would you manage a draft and an architecture of such an application?
What kind of technologies, would you like to use Why and Pros/Cons of such a decision.

In this case, you can just create a short document and send it to pavel@boataround.com and to miroslav.kostka@dev.boataround.com

## Last words ##

Thank you very much for the time you spend with this test, we really appreciate it! Good luck and in case of any questions/issues, please forward the e-mail to miroslav.kostka@dev.boataround.com